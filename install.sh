#!/bin/bash

CURRENT_DIR=`pwd`
BASH_PATHS="$CURRENT_DIR/bash:$CURRENT_DIR/bash/backend:$CURRENT_DIR/bash/mobile:$CURRENT_DIR/bash/web"

for profile in .bash_profile .zshenv
do
    if [[ `cat $HOME/$profile` != *"export PATH=$BASH_PATHS:\$PATH"* ]]; then
        printf "\nexport PATH=${BASH_PATHS}:\$PATH" >> $HOME/$profile
    fi
done
