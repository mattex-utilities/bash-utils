import sys
import re
import functions

projectName = sys.argv[-2]
filename = sys.argv[-1].rsplit('/', 1)[-1]
structName = re.sub(r'^(?:\d+_)*([^.]*).*', lambda pat: pat.group(1), filename)
structName = functions.snakeToCamel(structName)

print('package migrations\n')
print('type ' + structName + ' struct{}\n')
if projectName == 'mattex-tmm-micro' or projectName == 'contractor-micro' or projectName == 'mattex-supplier' or projectName == 'emat-micro' or projectName == 'mattex-mmm-v2':
    print('func (table *' + structName + ') Up(migrator *migrate.Migrator) {')
else:
    print('func (table *' + structName + ') Up(migrator *Migrator) {')
print('    migrator.DB.Exec(``)')
print('}\n')
if projectName == 'mattex-tmm-micro' or projectName == 'contractor-micro' or projectName == 'mattex-supplier' or projectName == 'emat-micro' or projectName == 'mattex-mmm-v2':
    print('func (table *' + structName + ') Down(migrator *migrate.Migrator) {')
else:
    print('func (table *' + structName + ') Down(migrator *Migrator) {')
print('    migrator.DB.Exec(``)')
print('}')
