import re

def snakeToCamel(source):
    source = re.sub(r'(?:^|_)([a-z])', lambda pat: pat.group(1).upper(), source)
    return source

def camelToSnake(source):
    source = re.sub(r'[A-Z]+', lambda pat: '_' + pat.group(0).lower(), source)
    source = re.sub(r'(?<!\w)_', r'', source)
    source = re.sub(r'(?<=_\w)_(?=\w(?:_|$))', r'', source)
    return source