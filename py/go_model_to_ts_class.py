import fileinput
import re
import functions

def typeConversion(source):
    source = re.sub(r'(?<!\w)int[0-9]*(?!\w)', r'number', source)
    source = re.sub(r'(?<!\w)float[0-9]*(?!\w)', r'number', source)
    source = re.sub(r'(?<!\w)string(?!\w)', r'string', source)
    source = re.sub(r'(?<!\w)bool(?!\w)', r'boolean', source)
    source = re.sub(r'(?<!\w)time\.Time(?!\w)', r'string', source)
    # list and pointer
    source = re.sub(r'\[\]\*?([^;]*)', lambda pat: pat.group(1) + '[],', source)
    source = re.sub(r': *\*([^;]*)', lambda pat: '?: ' + pat.group(1), source)
    return source

for line in fileinput.input():
    # skip ignored field
    if (re.search(r'(?:json|form):"-"', line)):
        continue
    line = re.sub(r'^[ \t]*', r'', line)
    # struct wrapper
    if (re.match(r'^ *type +(\w+) +struct +\{', line)):
        line = re.sub(r'^ *type +(\w+) +struct +\{', lambda pat: 'export interface ' + pat.group(1) + '{', line)
        print(line.rstrip())
        continue
    if (re.match(r'^\}[ \t]*', line)):
        line = re.sub(r'^\}[ \t]*', r'}', line)
        print(line.rstrip())
        continue
    # leading upper case chars
    line = re.sub(r'^([A-Z]+)([A-Z \t])', lambda pat: pat.group(1).lower() + pat.group(2), line)
    line = re.sub(r'^[A-Z]', lambda pat: pat.group(0).lower(), line)
    # format conversion
    line = re.sub(r'(\w+) +([^ ]*) +`[^`]*(?:json|form):"([^",]+)[,"][^`]*`', lambda pat: pat.group(3) + ': ' + pat.group(2) + ';', line)
    line = re.sub(r'^([A-Za-z]+)[ \t]+([^ \t\n]+).*', lambda pat: functions.camelToSnake(pat.group(1)) + ': ' + pat.group(2) + ';', line)
    # type conversion
    line = typeConversion(line)
    # remove ending comma of last line
    line = re.sub(r',(?!\n)', r'', line)
    print(line.rstrip())