import fileinput
import re

def convertFormat(source):
    source = re.sub(r'(%(?:[0-9]+\$)?)@', lambda pat: pat.group(1) + 's', source)
    source = re.sub(r'"', '\\"', source)
    return source

for line in fileinput.input():
    line = re.sub(r'^[ \t]*"(.*?)"[ \t]*="(.*?)".*', lambda pat: '<string name="' + pat.group(1) + '">' + convertFormat(pat.group(2)) + '</string>', line)
    print(line.rstrip())