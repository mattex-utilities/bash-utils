import fileinput
import re

def convertFormat(source):
    source = re.sub(r'(%(?:[0-9]+\$)?)[sdf]', lambda pat: pat.group(1) + '@', source)
    source = re.sub(r'"', '\\"', source)
    return source

for line in fileinput.input():
    line = re.sub(r'^[ \t]*<string name="([^"]+)"[^>]*>(.*)</string>', lambda pat: '"' + pat.group(1) + '"="' + convertFormat(pat.group(2)) + '";', line)
    print(line.rstrip())