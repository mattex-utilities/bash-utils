import fileinput
import re

def convertKey(source):
    source = re.sub(r'\.', lambda pat: '_', source)
    return source

for line in fileinput.input():
    line = re.sub(r'^[ \t]*([\'"])(.*)\1: *([\'"])(.*)\3.*', lambda pat: '<string name="' + convertKey(pat.group(2)) + '">' + pat.group(4) + '</string>', line)
    print(line.rstrip())