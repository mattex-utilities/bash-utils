import fileinput
import re

def typeConversion(source):
    source = re.sub(r'(?<!\w)Boolean', r'Bool', source)
    source = re.sub(r'(?<!\w)List<([^>]+)>', lambda pat: '[' + pat.group(1) + ']', source)
    return source

def initValueConversion(source):
    source = re.sub(r'(?<!\w)null', r'nil', source)
    return source

savedCodingKeys = []

for line in fileinput.input():
    if (re.match(r'^[ \t]*data +class +([^( ]+)\(', line)):
        line = re.sub(r'^[ \t]*data +class +([^( ]+)\(', lambda pat: 'struct ' + pat.group(1) + ": Codable {", line)
        print(line.rstrip())
        continue
    if (re.match(r'^[ \t]*\)', line)):
        break
    pat = re.search(r'^[ \t]*@field:Json\(name *= *"([^"]+)"\) +(?:override +)?(?:val|var) +([^:]+): *([^, ]+) *= *([^,]+),?', line)
    if pat:
        line = 'var ' + pat.group(2) + ": " + typeConversion(pat.group(3)) + " = " + initValueConversion(pat.group(4))
        if pat.group(1) != pat.group(2):
            savedCodingKeys.append('case ' + pat.group(2) + ' = "' + pat.group(1) + '"')
        else:
            savedCodingKeys.append('case ' + pat.group(2))
    pat = re.search(r'^[ \t]*@field:Json\(name *= *"([^"]+)"\) +(?:override +)?(?:val|var) +([^:]+): *([^,]+),?', line)
    if pat:
        line = 'var ' + pat.group(2) + ": " + typeConversion(pat.group(3))
        if pat.group(1) != pat.group(2):
            savedCodingKeys.append('case ' + pat.group(2) + ' = "' + pat.group(1) + '"')
        else:
            savedCodingKeys.append('case ' + pat.group(2))
    print('\t' + line.rstrip())
    # line = re.sub(r'^[ \t]*@field:Json\(name *= *"([^"]+)"\) +(?:val|var) +([^:]+): *([^, ]+) *= *([^,]+),?', lambda pat: 'var ' + pat.group(2) + ": " + typeConversion(pat.group(3)) + " = " + pat.group(4), line)
    # line = re.sub(r'^[ \t]*@field:Json\(name *= *"([^"]+)"\) +(?:val|var) +([^:]+): *([^,]+),?', lambda pat: 'var ' + pat.group(2) + ": " + typeConversion(pat.group(3)), line)
    # print(line.rstrip())

print('\n\tprivate enum CodingKeys: String, CodingKey {')
for savedLine in savedCodingKeys:
    print('\t\t' + savedLine)
print('\t}')
print('}')