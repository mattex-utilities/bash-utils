import fileinput
import re
import functions

def typeConversion(source):
    source = re.sub(r'(?<!\w)int[0-9]*(?!\w)', r'Int', source)
    source = re.sub(r'(?<!\w)float[0-9]*(?!\w)', r'Double', source)
    source = re.sub(r'(?<!\w)string(?!\w)', r'String', source)
    source = re.sub(r'(?<!\w)bool(?!\w)', r'Boolean', source)
    source = re.sub(r'(?<!\w)time\.Time(?!\w)', r'String', source)
    # list and pointer
    source = re.sub(r'\[\]\*?(.*),', lambda pat: 'List<' + pat.group(1) + '>?,', source)
    source = re.sub(r'\*(.*),', lambda pat: pat.group(1) + '?,', source)
    return source

def structNameCustomConversion(source):
    return re.sub(r'^([\w0-9]+)Response(?!\w)', lambda pat: pat.group(1), source)

for line in fileinput.input():
    # skip ignored field
    if (re.search(r'(?:json|form):"-"', line)):
        continue
    line = re.sub(r'^[ \t]*', r'', line)
    # struct wrapper
    if (re.match(r'^ *type +(\w+) +struct +\{', line)):
        line = re.sub(r'^ *type +(\w+) +struct +\{', lambda pat: 'data class ' + structNameCustomConversion(pat.group(1)) + '(', line)
        print(line.rstrip())
        continue
    if (re.match(r'^\}[ \t]*', line)):
        line = re.sub(r'^\}[ \t]*', r')', line)
        print(line.rstrip())
        continue
    # leading upper case chars
    line = re.sub(r'^([A-Z]+)([A-Z \t])', lambda pat: pat.group(1).lower() + pat.group(2), line)
    line = re.sub(r'^[A-Z]', lambda pat: pat.group(0).lower(), line)
    # format conversion
    line = re.sub(r'([\w\d]+) +([^ ]*) +`[^`]*(?:json|form):"([^",]+)(?:,[^"]+)?"[^`]*`', lambda pat: '@field:Json(name = "' + pat.group(3) + '") val ' + pat.group(1) + ': ' + structNameCustomConversion(pat.group(2)) + ',', line)
    line = re.sub(r'^([A-Za-z0-9]+)[ \t]+([^ \t\n]+).*', lambda pat: '@field:Json(name = "' + functions.camelToSnake(pat.group(1)) + '") val ' + pat.group(1) + ': ' + structNameCustomConversion(pat.group(2)) + ',', line)
    # type conversion
    line = typeConversion(line)
    # # remove ending comma of last line
    # line = re.sub(r',(?!\n)', r'', line)
    print(line.rstrip())