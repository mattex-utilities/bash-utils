# Bash Utils

## Prerequisite

- OSX System
- python 2.7

## Setup

To install the bash files, just run

```
./install.sh
```

This command will add directories of bash files to your PATH.

You need to start a new terminal session for the installation to be effective, or run

```
source ~/.bash_profile
```

for bash or

```
source ~/.zshenv
```

for zsh on your current terminal.

## Usage

### Backend Utils

#### `gen-migration-file`

![image gen-migration-file-demo](https://gitlab.com/mattex-utilities/bash-utils/-/raw/main/resource/gen-migration-file-demo.png)

In any Go project folder, you can run

```
gen-migration-file [MIGRATION-FILE-NAME-WITHOUT-DATETIME]
```

to immediately generate a migration file with struct & functions.

Some projects may require importing package of "migrate", you can just save on the generated file to auto import.

### Mobile Utils

Below bash files share the similar steps:

1. Copy source code to be converted.
2. Find any terminal, run the command to convert. The converted text should be automatically copied to your clipboard.
3. Paste directly to target IDE or text editor.

#### `go-model-to-kotlin-data`

![image go-model-to-kotlin-data](https://gitlab.com/mattex-utilities/bash-utils/-/raw/main/resource/go-model-to-kotlin-data.gif)

#### `kotlin-data-to-swift-struct`

![image kotlin-data-to-swift-struct](https://gitlab.com/mattex-utilities/bash-utils/-/raw/main/resource/kotlin-data-to-swift-struct.gif)

#### `react-string-to-android-string`

#### `android-string-to-ios-string`

![image react-to-mobile-string](https://gitlab.com/mattex-utilities/bash-utils/-/raw/main/resource/react-to-mobile-string.gif)

### Web Utils

#### `go-model-to-ts-class`

![image go-model-to-ts-class](https://gitlab.com/mattex-utilities/bash-utils/-/raw/main/resource/go-model-to-ts-class.gif)

### Other Utils

#### `docker-total-mem`

An alias command of docker to calculate memories of all docker containers running at the moment.

#### `gitfetchbranch`

```
gitfetchbranch [TARGET-BRANCH-NAME]
```

An alias command of git to fetch latest commit of target branch (which is "develop" by default). With this command, you will no longer need to switch branch.
